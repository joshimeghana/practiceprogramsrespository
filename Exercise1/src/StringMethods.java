public class StringMethods {

	static void strLength() {
		String s = "Java Program";
		int strlen = s.length();
		System.out.println("The Length of the String is: " + strlen);
	}

	static void splitString() {
		String s = "Java#program";
		String s1[] = s.split("#");
		System.out.println("String after split:");
		for (String a : s1) {
			System.out.println(a);
		}
	}

	static void replaceAll() {
		String s = "Java Program Java Classes Java Methods";
		String s1 = s.replaceAll("Java", "String");
		System.out.println("New String: " + s1);
	}

	static void charArray() {
		String s = "Java";
		char ch[] = s.toCharArray();
		for (char a : ch) {
			System.out.print(a + " ");
			System.out.println();
		}
	}

	static void charIndex() {
		String s = "Javaprogran";
		char res = s.charAt(3);
		System.out.println("Character at Index 3 is : " + res);
	}

	static void subString() {
		String s = "JavaProgram";
		String s1 = s.substring(0, 4);
		System.out.println(s1);
	}

	static void strMatch() {
		String s = "Java Strings";
		boolean s1 = s.matches("(.*)Str(.*)");
		System.out.println(s1);
	}
	
	static void strCompare() {
		String s1="Java";
		String s2="String";
		System.out.println(s1.compareTo(s2));
	}
	
	static void strEmpty() {
		String s="";
		System.out.println(s.isEmpty());
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		strLength();
		splitString();
		replaceAll();
		charArray();
		charIndex();
		subString();
		strMatch();
		strCompare();
		strEmpty();
	}

}
