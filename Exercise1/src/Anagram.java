///Write a method to Check if a string is an anagram
import java.util.Arrays;

public class Anagram {

	static void strAnagram() {
		String s1 = "Listen";
		String s2 = "silent";

		if (s1.length() == s2.length()) {

			char ch1[] = s1.toLowerCase().toCharArray();
			Arrays.sort(ch1);

			char ch2[] = s2.toLowerCase().toCharArray();
			Arrays.sort(ch2);

			boolean Status = Arrays.equals(ch1, ch2);

			if (Status) {
				System.out.println("Given Strings are Anagrams");
			} else {
				System.out.println("Given Strings are not Anagrams");
			}
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		strAnagram();
	}

}
