//Write a method that checks if the string passed is a palindrome or not(return true if its a palindrome else false).
public class Palindrome {
	static boolean strPalindrome() {
		String S1 = "MaDam";
		String S2 = "";

		for (int i = S1.length() - 1; i >= 0; i--) {
			S2 = S2 + S1.charAt(i);
		}
		if (S2.equalsIgnoreCase(S1)) {
			return true;
		} else {
			return false;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean res = strPalindrome();
		if (res == true) {
			System.out.println("Given string is palindrome");
		} else {
			System.out.println("Given string not palindrome");
		}
	}
}
