package test;

public class FindSubString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int res = isSubstring("world", "Hello world");
		if (res == -1) {
			System.out.println("Not present");
		} else {
			System.out.println("Present at index:" + res);
		}
	}

	static int isSubstring(String s1, String s2) {
		
		
	int s1len = s1.length();
		//System.out.println("S1 length: " + s1len);
		int s2len = s2.length();
		//System.out.println("S2 length: " + s2len);
		//System.out.println();
		for (int i = 0; i <= s2len - s1len; i++) {// 0<2//1<2//2<=2 
			//System.out.println("i:" + i);
			int j = 0;
			for (; j < s1len; j++) {// 0<2//0<2//0<2//1<2//2<2
				//System.out.println("j:" + j);
				//System.out.println();
				if (s2.charAt(i + j) != s1.charAt(j)) {// j!=v //a!=v//v=v//a=a
					break;
				}
			}
			//System.out.println(j);
			if (j == s1len) {// 1==2
				return i;
			}
		}
		return -1;
	}
}
