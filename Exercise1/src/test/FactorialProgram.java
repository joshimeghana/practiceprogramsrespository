package test;

public class FactorialProgram {

	static void factorialIteration() {
		int fact = 1;
		int num = 4;

		for (int i = 1; i <= num; i++) {
			fact = fact * i;
		}
		System.out.println("Factorial(Iteration) of: " + fact);
	}

	// 5!=5*4*3*2*1
	static int factorialRecurssion(int n) {
		if (n == 1) {
			//System.out.println("factorial(1)");
			return 1;
		} else {
			//System.out.println(+n + "*" + (n - 1));
			return n * (factorialRecurssion(n - 1));
		}
	}
	
	public static void sayHi(int n) {
		if (n == 1) {
			System.out.println("Done"); ;
		} else {
			System.out.println("hi");
			n--;
			sayHi(n);
		}
	}
	public static int nineTable(int n,int i)
	{
		if(i>10)
		{
			return 0;
		}else
		{
			System.out.println(n*i);
			return nineTable(n, i+1);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		factorialIteration();
		int res = factorialRecurssion(5);
		System.out.println("Factorial Recurssion: "+res);
		sayHi(5);
		
		nineTable(9, 1);
		
		
	}
}
