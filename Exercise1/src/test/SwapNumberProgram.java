package test;

public class SwapNumberProgram {
	public static void swapNumber()
	{
		int a=43;
		int b=25;
		
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println(a+" "+b);
	
	}
	public static void swapTemp()
	{
		int a=25;
		int b=34;
		
		int temp=a;
		a=b;
		b=temp;
		
		System.out.println(a+ " "+b);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		swapNumber();
		swapTemp();
	}

}
