package test;

public class MergedArray {

	static void arrayMerge() {
	
		int a[] = { 15, 14, 88, 65, 25 };
		int b[] = { 87, 56, 24, 36, 21 };

		int c[] = new int[a.length + b.length];

		for (int i = 0; i < a.length; i++) {
			c[i] = a[i];
		}
		int k = a.length;
		for (int i = 0; i < b.length; i++) {
			c[k] = b[i];
			k++;
		}
		int temp = 0;
		for (int i = 0; i < c.length; i++) {
			// System.out.print("i: "+i+ " ");
			// System.out.println();
			for (int j = i + 1; j < c.length; j++) {
				// System.out.print("j: "+j + " ");
				if (c[i] > c[j]) {
					temp = c[i];
					c[i] = c[j];
					c[j] = temp;
				}
			}
		}
		for (int m : c) {
			System.out.print(m + " ");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		arrayMerge();
	}
}
