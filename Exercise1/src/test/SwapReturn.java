package test;

public class SwapReturn {

	static int[] swapwithoutTemp(int a, int b)
	{
		int ans[]=new int[2];
		//a=4 b=2
		a=a+b;//4+2=6 :: a=6;
		b=a-b;//6-2=4 :: b=4;
		a=a-b;//6-4=2 :: a=2; 
		
		ans[0]=a;
		ans[1]=b;
		
		return ans;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int res[]=swapwithoutTemp(4, 2);
		for(int k:res)
		{
			System.out.println(k);
		}
	}

}
