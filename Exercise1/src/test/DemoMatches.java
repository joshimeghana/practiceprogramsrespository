package test;

public class DemoMatches {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = new String("Java String Methods");

		System.out.print("Regex: (.*)String(.*) matches string? ");
		System.out.println(str.matches("(.*)String(.*)")); 	

		System.out.print("Regex: (.*)Strings(.*) matches string? ");
		System.out.println(str.matches("(.*)Strings(.*)"));

		System.out.print("Regex: (.*)Methods matches string? ");
		System.out.println(str.matches("(.*)Methods"));
	}

}
