package com.test.stringprograms;

public class DemoReplaceall {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = new String("Java classes Java methods");
		System.out.println("Replacing words:");
		System.out.println(str.replaceAll("Java", "Strings"));
		System.out.println();
		System.out.println("Replacing whole String:");
		System.out.println(str.replaceAll("(.*)cla(.*)", "Strings methods in java"));
	}
}
