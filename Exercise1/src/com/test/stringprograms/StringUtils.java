package com.test.stringprograms;

import java.util.HashMap;
import java.util.Map;

public class StringUtils {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	static int stringtoNumber(String s) {
		int i = Integer.parseInt(s);
		return i;
	}

	static int strtoNumber(String s) {
		int num = 0;
		int zeroascii = (int) '0';

		for (int i = 0; i < s.length(); i++) {
			int tempascii = (int) s.charAt(i);
			num = num * 10 + (tempascii - zeroascii);
		}
		return num;
	}

	static int addnumbersString(String s) {

		s = s.replaceAll("\\D+", "");
		int num = Integer.parseInt(s);

		int mod = 0;
		int sum = 0;

		while (num > 0) {
			mod = num % 10;
			sum = sum + mod;
			num = num / 10;
		}
		return sum;
	}

	static String[] reverseString(String s) {
		String s1[] = s.split(" ");

		return s1;
	}

	static int isSubstring(String s1, String s2) {

		int s1len = s1.length();
		int s2len = s2.length();

		for (int i = 0; i <= s2len - s1len; i++) {
			int j = 0;
			for (; j < s1len; j++) {
				if (s2.charAt(i + j) != s1.charAt(j)) {
					break;
				}
			}

			if (j == s1len) {
				return i;
			}
		}

		return -1;
	}

	static Map<Character, Integer> countDuplicates(String s1) {
		Map<Character, Integer> s2 = new HashMap<Character, Integer>();
		for (int i = 0; i < s1.length(); i++) {
			char ch = s1.charAt(i);
			if (s2.containsKey(ch)) {
				s2.put(ch, s2.get(ch) + 1);
			} else {
				s2.put(ch, 1);
			}
		}
		return s2;
	}

	static boolean demoMatches(String s) {
		String str = new String("Java String Methods");

		boolean res = str.matches("(.*)String(.*)");

		return res;
	}

	public static String[] splitStr(String s) {
		String s1[] = s.split("/");

		return s1;
	}

}
