package com.test.stringprograms;

import java.util.StringTokenizer;

public class DemoStringTokenizer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StringTokenizer str = new StringTokenizer("Java is a programming language", " ");

		System.out.println("Number of tokens in the given String:" + str.countTokens());

		while (str.hasMoreTokens()) {
			System.out.println(str.nextToken(" "));
		}
	}
}
