package com.test.firstset;

public class Utils {

	public static int[] swapwithoutTemp(int a, int b) {
		a = a + b;
		b = a - b;
		a = a - b;

		int ans[] = new int[2];
		ans[0] = a;
		ans[1] = b;

		return ans;
	}

	public static int[] swapwithTemp(int a, int b) {

		int temp = a;
		a = b;
		b = temp;

		int ans[] = new int[2];
		ans[0] = a;
		ans[1] = b;

		return ans;
	}

	public static int factorialIteration(int num) {
		int fact = 1;
		for (int i = 1; i <= num; i++) {
			fact = fact * i;
		}
		return fact;
	}

	public static int factorialRecurssion(int num) {
		if (num == 1) {
			return 1;
		} else {
			return num * factorialIteration(num - 1);
		}
	}

	public static boolean primeNumber(int num) {
		boolean flag = true;
		for (int i = 2; i < num - 1; i++) {
			if (num % i == 0) {
				flag = false;
				break;
			}
		}
		if (flag) {
			return true;
		} else {
			return false;
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
