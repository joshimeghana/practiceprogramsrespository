package com.test.finalfirstset;

public class PatternPrint {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		printPattern(5);
	}
	static void printPattern(int rows)
	{
		for(int i=1;i<=rows;i++)
		{
			for(int j=1;j<=i;j++)
			{
				System.out.print(j+" ");
			}
			System.out.println();
		}
		
		for(int i=rows-1;i>=1;i--)
		{
			for(int j=1;j<=i;j++)
			{
				System.out.print(j+" ");
			}
			System.out.println();
		}
	}
}
