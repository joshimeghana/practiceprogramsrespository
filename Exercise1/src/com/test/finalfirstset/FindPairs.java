//Find pairs: Given an array ( {3,4,5,7,2,8,9,10,6} and given a number ex:10 
//find the pairs in the array whose total is 10 (Ex the pairs are :3:7,4:6,2:8}

package com.test.finalfirstset;

public class FindPairs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[] = { 3, 4, 5, 7, 2, 8, 9, 10, 6 };
		int b = 10;
		findPairs(a,b);
	}
	static void findPairs(int a[],int b)
	{
		for(int i=0;i<a.length;i++)
		{
			for(int j=i+1;j<a.length;j++)
			{
				if(a[i]+a[j]==b)
				{
					System.out.println(a[i]+" "+a[j]);
				}
			}
		}
	}

}
