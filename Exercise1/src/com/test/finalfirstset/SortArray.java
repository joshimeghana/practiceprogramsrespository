package com.test.finalfirstset;

public class SortArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		arraySort();
	}

	static void arraySort() {
		int a[] = { 87, 69, 45, 32, 78 };

		for (int i = 0; i < a.length; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] > a[j]) {
					int temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		for (int kk : a) {
			System.out.print(kk + " ");
		}
	}
}
