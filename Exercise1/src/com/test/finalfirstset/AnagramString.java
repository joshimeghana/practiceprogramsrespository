package com.test.finalfirstset;

import java.util.Arrays;

public class AnagramString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		stringAnagram();
	}
	static void stringAnagram()
	{
		String s1="listen";
		String s2="Silent";
		
		if(s1.length()==s2.length())
		{
			char c1[]=s1.toLowerCase().toCharArray();
			Arrays.sort(c1);
			
			char c2[]=s2.toLowerCase().toCharArray();
			Arrays.sort(c2);
			
			boolean res=Arrays.equals(c1, c2);
			
			if(res==true)
			{
				System.out.println("Strings are anagram");
			}else {
				System.out.println("Strings are not anagram");
			}
		}else {
			System.out.println("String length doesnt match");
		}
	}
}
