package com.test.finalfirstset;

public class PrimeNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		isPrime(2);
		isPrime(9);
	}

	static void isPrime(int num) {
		int flag = 0;

		for (int i = 2; i <= num - 1; i++) {
			if (num % i == 0) {
				flag = 1;
				break;
			}
		}
		if (flag == 1) {
			System.out.println("The given number is not Prime Number");
		} else {
			System.out.println("The given number is Prime Number");
		}

	}

}
