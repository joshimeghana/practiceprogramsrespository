//Write a method that takes an array of numbers as first argument and any number as the second argument. 
//The method returns true if the number is present in the array else returns false (Perform Search using Linear and Binary).

package com.test.finalfirstset;

public class FindNuminArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[] = { 12, 6, 5, 4, 8 };
		int b = 4;
		int res = findNum(a, b);
		if (res != 0) {
			System.out.println("Number found in position:" + res);
		} else {
			System.out.println("Number not found");
		}
	}

	static int findNum(int a[], int b) {
		for (int i = 0; i < a.length; i++) {
			if (a[i] == b) {
				return i;
			}
		}
		return 0;
	}
}
