package com.test.finalfirstset;

import java.util.Arrays;

public class RemoveDuplicates {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[] = { 11, 25, 25, 78, 36, 45 };
		removeDuplicates(a);  //8888
	}

	static void removeDuplicates(int a[]) {
		int temp = 0;
		int len = a.length;
		for (int i = 0; i < len; i++) {
			for (int j = i + 1; j < len; j++) {
				if (a[i] == a[j]) {
					temp = a[j];
					a[j] = a[len - 1];
					a[len - 1] = temp;
					len--;
					j--;
				}
			}
		}
		int res[] = Arrays.copyOf(a, len);
		for (int m : res) {
			System.out.print(m + " ");
		}
	}

}
