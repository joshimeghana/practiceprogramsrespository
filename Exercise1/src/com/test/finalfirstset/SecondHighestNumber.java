package com.test.finalfirstset;

public class SecondHighestNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[] = { 14, 15, 22, 3, 68 };
		int res = secondHighestNo(a);
		System.out.println("Second Highest Number is: " + res);
	}

	static int secondHighestNo(int a[]) {
		int temp = 0;
		for (int i = 0; i < a.length; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] > a[j]) {
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		return a[a.length - 2];
	}
}
