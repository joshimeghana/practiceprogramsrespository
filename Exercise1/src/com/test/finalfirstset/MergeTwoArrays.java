package com.test.finalfirstset;

public class MergeTwoArrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		mergeArray();
	}
	static void mergeArray()
	{
		int a[]= {10,65,89,74,41};
		int b[]= {78,95,65,47,12};
		
		int c[]=new int[a.length+b.length];
		
		for(int i=0;i<a.length;i++)
		{
			c[i]=a[i];
		}
		int k=a.length;
		for(int i=0;i<b.length;i++)
		{
			c[k]=b[i];
			k++;
		}
		int temp=0;
		for(int i=0;i<c.length;i++)
		{
			for(int j=i+1;j<c.length;j++)
			{
				if(c[i]>c[j])
				{
					temp=c[i];
					c[i]=c[j];
					c[j]=temp;
				}
			}
		}
		for(int kk:c)
		{
			System.out.print(kk+ " ");
		}
	}

}
