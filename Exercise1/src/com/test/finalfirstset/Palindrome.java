package com.test.finalfirstset;

public class Palindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		boolean res = isPalindrome();

		if (res == true) {
			System.out.println("String is Palindrome");
		} else {
			System.out.println("String is not Palindrome");
		}
	}

	static boolean isPalindrome() {

		String s1 = "madam";
		String s2 = "";

		for (int i = s1.length() - 1; i >= 0; i--) {
			s2 = s2 + s1.charAt(i);
		}

		if (s2.equalsIgnoreCase(s1)) {
			return true;
		} else {
			return false;
		}

	}
}
