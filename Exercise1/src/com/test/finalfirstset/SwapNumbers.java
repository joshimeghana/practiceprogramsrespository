package com.test.finalfirstset;

public class SwapNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		swapwithoutTemp(8, 10);
		swapwithTemp(25, 50);
	}

	static void swapwithoutTemp(int a, int b) {

		a = a + b;
		b = a - b;
		a = a - b;

		System.out.println(a + " " + b);
	}

	static void swapwithTemp(int a, int b) {

		int temp = a;
		a = b;
		b = temp;

		System.out.println(a + " " + b);
	}
}
