package com.test.finalfirstset;

public class SmallBigNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[] = { 12, 3, -45, 147, 89, 0, -98 };
		smallbigNumber(a);
	}

	static void smallbigNumber(int a[]) {
		int smallest = 0;
		int biggest = 0;
		
		for (int i = 0; i < a.length; i++) {
			if (a[i] > 0) {
				smallest = a[i];
			}
		}
		for (int i = 0; i < a.length; i++) {
			if (a[i] < smallest && a[i] > 0) {
				smallest=a[i];
			}
		}
		System.out.println("Smallest Positive Number is: "+ smallest);
		
		
		for(int i=0;i<a.length;i++)
		{
			if(a[i]>0)
			{
				biggest=a[i];
			}
		}
		for(int i=0;i<a.length;i++)
		{
			if(a[i]>biggest&&a[i]>0)
			{
				biggest=a[i];
			}
		}
		System.out.println("Biggest Positive Number is: "+biggest);
	}
}
