package com.test.finalfirstset;

public class Fibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		fibonacci(10);
	}

	static void fibonacci(int num) {

		int fib[] = new int[num];
		fib[0] = 0;
		fib[1] = 1;

		for (int i = 2; i < num; i++) {
			fib[i] = fib[i - 2] + fib[i - 1];
		}
		for (int i = 0; i < num; i++) {
			System.out.print(fib[i] + " ");
		}
	}

}
