package com.test.javaprograms;

import java.util.Arrays;

public class StringArrayUtil {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static int findNo(int a[]) {
		int missingno = 0;

		for (int i = 0; i < a.length - 1; i++) {
			int nextno = a[i] + 1;
			if (a[i + 1] != nextno) {
				missingno = nextno;
			}
		}
		return missingno;
	}

	public static int[] removeDup(int a[]) {
		int temp = 0;
		int len = a.length;

		for (int i = 0; i < len; i++) {
			for (int j = i + 1; j < len; j++) {
				if (a[i] == a[j]) {
					temp = a[j];
					a[j] = a[len - 1];
					a[len - 1] = temp;
					len--;
					j--;
				}
			}
		}
		int a1[] = Arrays.copyOf(a, len);

		return a1;

	}

	public static String reverseStr(String str) {

		String reversedStr = " ";
		String midstr1 = " ";
		String midstr2 = " ";

		String str1[] = str.split(" ");

		int strlen = str1.length;

		for (int i = 0; i < strlen / 2; i++) {
			midstr1 = midstr1 + str1[i] + " ";
		}

		for (int j = strlen - 2; j < strlen; j++) {
			midstr2 = midstr2 + str1[j] + " ";
		}
		midstr2 = midstr2.trim();

		reversedStr = midstr2 + midstr1;

		return reversedStr;
	}

}
